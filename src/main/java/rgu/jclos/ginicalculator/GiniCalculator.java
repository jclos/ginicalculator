/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rgu.jclos.ginicalculator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Jeremie
 */
public class GiniCalculator {
    public static void main(String[] args) {
        
        List<Double> data = new ArrayList<>();
 


        data.add(-0.89d);
        data.add(-0.89d);


        Collections.sort(data);
        System.out.println(calculateOrdered(shiftData(data)));  
    }
    
    /**
     * Shift the potentially negative data so that Gini can be computed on a positive collection.
     * @param data the dataset
     * @return the transformed dataset
     */
    public static Collection<Double> shiftData(Collection<Double> data)
    {
        final double min = data.stream().min((a, b) -> Double.compare(a, b)).get();
        if(min < 0){
            return data.stream().map(x -> x + Math.abs(min)).collect(Collectors.toList());
        } else {
            return data;
        }
    }
    
    
    /**
     * Calulcates the Gini coefficient on an ordered dataset
     * @param data an ordered dataset
     * @return the Gini coefficient
     */
    public static double calculateOrdered(Collection<Double> data) {
        double rectification = 0d;
        if (data.stream().allMatch(x -> x == 0))
        {
            rectification = 0.0000000001d;
        }
        
        double numerator = 0d;
        int i = 1;
        int n = data.size();
        for (double xi : data) {
            numerator += ((2 * i - n - 1) * xi);
            i++;
        }
        double denominator2 = 0d;
        for(double xi : data) {
            denominator2 += xi;
        }
        double denominator = Math.pow(data.size(), 2) * (denominator2/data.size());
        return (double) numerator / (double) (denominator + rectification);
    }
    
    /**
     * Calculates the Gini coefficient on an unordered dataset. Caution: there is
     * a double loop inside, so the operation is at best quadratic.
     * @param data an unordered dataset
     * @return the Gini coefficient
     */
    public static double calculateUnordered(Collection<Double> data) {
        double rectification = 0d;
        if (data.stream().allMatch(x -> x == 0))
        {
            rectification = 0.0000000001d;
        }
        
        double numerator = 0d;
        for(double xi : data) {
            for(double xj : data) {
                numerator += Math.abs(xi - xj);
            }
        }
        double denominator2 = 0d;
        for(double xi : data) {
            denominator2 += xi;
        }
        double denominator = 2 * Math.pow(data.size(), 2) * (denominator2/data.size());
        return (double) numerator / (double) (denominator + rectification);
    }
}
